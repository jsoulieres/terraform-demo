variable "aws_region" {
  type = "string"
  description = "The AWS region to use"
  default = "us-east-1"
}

variable "aws_access_key" {
  description = "AWS Access Key"
  default = "AKIAUZXKVNYMT32QRZWI"
}

variable "aws_secret_key" {
  description = "AWS Secret Key"
  default = "wHTZEIKYjpsZkI4Nq5bJlFPxOOG95tMAfJsgx2dz"

}

variable "authorized_key" {
  type = "string"
  default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDPBeJfL/lWhqwEb2J1G5Ez46PksvnY6+THBXJWR/DLmLkIer5Uras9KeP6Cbyn+N1YE94raChzxcNGxCd+qYmWZ5I87jf3VLe3vY3NZ3zp+aX4fQ5IIJLo/5xnX/ynDTPS98zf85ygPpbl+xpuixFTCyEOWz98h7Fd9yleKyCaSVNNblVSCWKPLHgtpxXZN1Nm3TpwTErKfWY/cBigr1GzLxUDccAB6GrFLUmJHH17uVHMMbdvBOifTIOWLW/RxKZJIBCRbzMu0ZyY6UmkiM/D7hs1nLpeI/VSqRYLDoPopDsK/zwmp+Gl+hwfL5UAzzFXib/7z6ZDkoEseBDYQAmoUa2J7Cs0BA/Qatksd7Cj9Wf8lVbBlsDRUp+aRHw0qCEpk4Zpnm381m2/6pM7mJ1RSIDCpre2cnn10256frSVGGUo2BDbmDovGmBGS0ptCU8P1Ro9UpBXw6jQzpVyPBjXH1DkryC0D9d+kAWAuSG5uyQOYGmfFtyd9mLZpliutUov1YG0W3PVU7cev9VwDwTDDMo3qbT4x+f2PBuKw0cEGHCNZLJVI8EMaO6QrQnjqbFm5cDHzEl7RMZU/+KlBiqDlcwZusDeAfocWrL0aVveQ2neqGMASEKqq7J8kifSXS1srn4QVrFlrl/JbtLJjcYzs1UFrBvez7dsIa4xqGz0Zw== spark-user@baseImage"
}

variable "instances_type" {
  default = "t2.micro"
}

variable "instances_count" {
  default = 3
}

variable "tags" {
  type = "map"
  default = {
    demo = true
  }
}
